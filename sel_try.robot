*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${LOGIN URL}    https://www.amazon.it/
${BROWSER_GHOST}    headlesschrome
${BROWSER}    chrome
${THING}    birra

*** Test Cases***
cerca oggetto su amazon
	Open Browser    ${LOGIN URL}    ${BROWSER_GHOST}
	#Go to    ${LOGIN URL}
	Input Text    twotabsearchtextbox    ${THING}
	Click Element    //*[@value="VAI"]
	${THING}    Get Text    //*[@id="s-result-count"]/span/span
	[Teardown]    Close Browser
